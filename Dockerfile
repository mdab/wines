FROM openjdk:8-jre
MAINTAINER Marcin Dabrowski <m.dab@hotmail.com>

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/wines/wines.jar"]
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/wines/wines.jar
EXPOSE 8080