package wines.repositories;

import java.util.UUID;

import wines.entities.Wine;
import org.springframework.data.cassandra.repository.CassandraRepository;


public interface WineRepository extends CassandraRepository<Wine, UUID> {

}
