package wines.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wines.entities.Wine;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datastax.driver.core.utils.UUIDs;
import wines.repositories.WineRepository;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")

public class WineServiceController {

    @Autowired
    WineRepository wineRepository;

    static Logger log = LogManager.getLogger("Wines Controller logger");

    @GetMapping("/wines")
    public List<Wine> getAllWines() {
        log.debug("Returning all wines.");
        return wineRepository.findAll();
    }

    @GetMapping("/wines/{id}")
    public ResponseEntity<Wine> getWine(@PathVariable("id") UUID id) {
        Optional<Wine> wineData = wineRepository.findById(id);
        if (wineData.isPresent()) {
            Wine wine = wineData.get();
            log.debug("Get wine: " + wine);
            return new ResponseEntity<>(wine, HttpStatus.OK);
        } else {
            log.error("Wine with id " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/wines/create")
    public ResponseEntity<Wine> createWine(@Valid @RequestBody Wine wine) {
        wine.setId(UUIDs.timeBased());
        Wine _wine = wineRepository.save(wine);
        log.debug("Created wine: " + _wine);
        return new ResponseEntity<>(_wine, HttpStatus.OK);
    }

    @PutMapping("/wines/{id}")
    public ResponseEntity<Wine> getWine(@PathVariable("id") UUID id, @RequestBody Wine wine) {
        Optional<Wine> wineData = wineRepository.findById(id);
        if (wineData.isPresent()) {
            Wine savedWine = wineData.get();
            savedWine.setName(wine.getName());
            savedWine.setPrice(wine.getPrice());
            savedWine.setAlcohol(wine.getAlcohol());
            savedWine.setBottleCapacityInMilliliters(wine.getBottleCapacityInMilliliters());

            Wine updatedWine = wineRepository.save(savedWine);
            log.debug("Updated wine: " + updatedWine);
            return new ResponseEntity<>(updatedWine, HttpStatus.OK);
        } else {
            log.error("Wine with id " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/wines/{id}")
    public ResponseEntity<String> deleteWine(@PathVariable("id") UUID id) {
        try {
            wineRepository.deleteById(id);
        } catch (Exception e) {
            log.error("Failed to delete wine with id " + id);
            return new ResponseEntity<>("Fail to delete!", HttpStatus.EXPECTATION_FAILED);
        }
        log.debug("Deleted wine with id " + id);
        return new ResponseEntity<>("Wine has been deleted!", HttpStatus.OK);
    }


}
