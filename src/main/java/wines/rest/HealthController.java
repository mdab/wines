package wines.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/")
public class HealthController {
    static Logger log = LogManager.getLogger("Wines Controller logger");

    @GetMapping("/health")
    public ResponseEntity returnHealthStatus() {
        log.info("Received the health check on /health endpoint.");
        return new ResponseEntity(HttpStatus.OK);
    }
}
