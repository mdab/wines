package wines.entities;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.math.BigDecimal;
import java.util.UUID;

@Table
public class Wine {

    @PrimaryKey
    private UUID id;

    private String name;
    private BigDecimal price;
    private Float alcohol;
    private Integer bottleCapacityInMilliliters;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Float getAlcohol() {
        return alcohol;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setAlcohol(Float alcohol) {
        this.alcohol = alcohol;
    }

    public Integer getBottleCapacityInMilliliters() { return bottleCapacityInMilliliters; }

    public void setBottleCapacityInMilliliters(Integer bottleCapacityInMilliliters) { this.bottleCapacityInMilliliters = bottleCapacityInMilliliters; }

    @Override
    public String toString(){
        return "[" + getId() + ":" + getName() + "]";
    }
}
